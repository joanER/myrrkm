      program rrkm
c
c     calculation of RRKM rate constants
c     with QM tunnelling using an Eckart barrier simulation
c
      implicit double precision (a-h,o-z)
      parameter (nemax = 25000, nenmax = 70000, nfree = 400) ! nfree must be longer than num freqs
      parameter (pcon = 6.62606896d-34) ! plancks constant
      parameter (wtoe = 1.986445d-23)   ! ???
      parameter (ghztoe = 6.62606896d-25)
      parameter (htoj = 4.359744d-18)
      parameter (rkjmtoj = 1.d0 / (6.022d20) ) 
      parameter (amu = 1.66053892d-27) 
      parameter (bcon = 1.3806488d-23) ! boltzmann constant
      dimension en(-nenmax:nemax),freqt(nfree),freqr(nfree)
      dimension denst(-nenmax:nemax),densr(-nenmax:nemax)
      dimension const1(-nenmax:nemax),const2(-nenmax:nemax)
      dimension denstot(-nenmax:nemax),jmax(-nenmax:nemax)
      dimension densttot(-nenmax:nemax)
      character *50 namt, namr, namout
      pi = dacos(-1.d0)
c
c     data input
c
      do i = -nenmax,nemax
        densttot(i) = 0.d0
        denstot(i) = 0.d0
        denst(i) = 0.d0
        densr(i) = 0.d0
      enddo
      read (5,*) namt   ! name ts
      read (5,*) namr   ! name react
      read (5,*) namout ! name out
      read (5,*) ezero  ! reference
      read (5,*) eoff   ! offset
c      open (2,file='out.capt',status='unknown') ! Capture
       do ie = 1, 3000          ! MODIFY choose the min and max energies
c          read (2,*) eg,jmax(ie)
          jmax(ie) = 0 
       enddo
c      close (2)
c
c     TS energy, vibrational frequencies, RCs and symmetry factors 
c     fim is the imaginary frequency, used for tunnelling
c 
      rct = 0.d0 ! here u sum -log(symm)-0.5*log(rot constant)
c
c     OPEN ts file and read E, # FRQs_ts, then
c      set the energy wrt reference and offset.
c      The first FREQ will be automatically 
c      stored as the imaginary one.
c
      open (1, file = namt, status='unknown')
      read (1,*) tsen,nfrt      
      tsen = htoj * (tsen + eoff - ezero) 
      read (1,*) fim 
      fim = fim * wtoe ! conversion of freq units
      do i = 1,nfrt
         read (1,*) freqt(i)
         freqt(i) = freqt(i) * wtoe ! conversion of freq units
      enddo
      read (1,*) ntran1t,nrot1t,nrot2t  ! transitional & rot constants
      if (ntran1t .gt. 0) then          ! transl
         do i = 1, ntran1t
            read (1,*) rm1t
            rm1t = rm1t * amu
            rct = rct + 0.5d0 * dlog(dble(rm1t))
         enddo
      endif
      if (nrot1t .gt. 0) then ! rotat 1
         do i = 1,nrot1t
            read (1,*) sr1t,isigt ! read constant and symmetry
            sr1t = sr1t * ghztoe  ! convert units
            rct = rct - dlog(dble(isigt)) - 0.5d0*dlog(sr1t)
         enddo
      endif
      if (nrot2t .gt. 0) then ! rotat 2
         do i = 1,nrot2t
            read (1,*) sr2t1, sr2t2, isigt ! read constant and symmetry
            sr2t = dsqrt(sr2t1 * sr2t2) * ghztoe
            rct = rct - dlog(dble(isigt)) - dlog(sr2t)
         enddo    
      endif
      read (1,*) bt1,bt2,isigt ! J constants
      rct = rct - dlog(dble(isigt))
      bt = dsqrt (bt1 * bt2)
      bt = bt * ghztoe
      close (1)
c
c     Reactant energy, vibrational frequencies, RCs and symmetry factors
c     Now the same as before but for the Reactants (i.e. no imag frq)
c
      rcr = 0.d0
      open (2, file = namr, status='unknown')
      read (2,*) ren,nfrr ! reactant energy and freqs
      ren = htoj * (ren + eoff - ezero)
c      if (ren .ge. tsen) stop 'Reactants higher than transition state!'
!
!     Below v0 is E(TS-React) <--> the barrier's height
!      pcon = planck's constant
!      convert freq from Hz to rad s-1: w = 2pi*f
      v0 = tsen - ren
      bortm = (pi * fim) / (pcon*dsqrt(v0/8.d0)) ! ??  tunneling related?
      do i = 1,nfrr ! read freqs
         read (2,*) freqr(i)
         freqr(i) = freqr(i) * wtoe
      enddo
      read (2,*) ntran1r,nrot1r,nrot2r ! trans, rot1 and rot2
      if (ntran1r .gt. 0) then
         do i = 1, ntran1r
            read (2,*) rm1r
            rm1r = rm1r * amu
            rcr = rcr + 0.5d0 * dlog(dble(rm1r))
         enddo
      endif
      if (nrot1r .gt. 0) then ! rot 1
         do i = 1,nrot1r
            read (2,*) sr1r,isigr
            sr1r = sr1r * ghztoe ! read rot constant 1 and its sym
            rcr = rcr - dlog(dble(isigr)) - 0.5d0 * dlog(sr1r)
         enddo
      endif
      if (nrot2r .gt. 0) then ! rot 2
         do i = 1,nrot2r
            read (2,*) sr2r1,sr2r2,isigr
            sr2r = dsqrt(sr2r1 * sr2r2) * ghztoe
            rcr = rcr - dlog(dble(isigr)) - dlog(sr2r)
         enddo   
      endif
      read (2,*) br1,br2,isigr ! J constants
      rcr = rcr - dlog(dble(isigr))
      br = dsqrt (br1 * br2)
      br = br * ghztoe
      close (2)
c #####################################################################
c        END READING - END READING - END READING 
c #####################################################################
c
c     logarithms of rotranslational densities of states for TS and reactants
c     (these are linear functions of the log of the energy) rho(E)=rct*E^{rst}
c
c     If undoneand only for rotations
c       rcr = log( Gamma(3/2)sqrt(pi)/(sigmaA*sigmaBC*sqrt(ABC))
c     and
c       rsr = 1/2
c                     
      rst = dble(nrot2t)+0.5d0*dble(nrot1t)+0.5d0*dble(ntran1t) - 1.d0
      rct = rct + 0.5d0 * dble(nrot1t) * dlog(pi) 
     +  - dlgam(dble(nrot2t) + 0.5d0*dble(nrot1t) + 0.5d0*dble(ntran1t))
      rct = rct + 0.5d0 * dble(ntran1t) * dlog(2.d0/(pcon**2))
      rsr = dble(nrot2r)+0.5d0*dble(nrot1r)+0.5d0*dble(ntran1r) - 1.d0
      rcr = rcr + 0.5d0 * dble(nrot1r) * dlog(pi) 
     +  - dlgam(dble(nrot2r) + 0.5d0*dble(nrot1r) + 0.5d0*dble(ntran1r))
      rcr = rcr + 0.5d0 * dble(ntran1r) * dlog(2.d0/(pcon**2))
c
c     energy grid
c
      emin = ren ! reactants energy
      read (5,*) ein,de,nepo ! read from input init, step and max Energies
      ein = ein * rkjmtoj    ! convert kJ/mol to J
      de = de * rkjmtoj 
      ist = int((ein - emin) / de) ! compute real init E according to Ref?
      nv0 = nint (v0 / de)   ! v0 = tsen - ren ==> n_steps
      nep = nepo + nv0       ! default: nep (numb energy points) is nepo + v0
      if (nv0 .lt. 0) nep = nepo ! modify default if v0 < 0
      if (nep .gt. nemax) stop 'Too low energies!' 
      if (ist .gt. nenmax) stop 'Too high energies!'
      do i = -ist + 1,nep
         en(i) = ein + de * (i-1)
         const1(i) = 0.d0
         const2(i) = 0.d0
      enddo
c
c     rotational densities/sums of states
c
      do ie = 1, nepo ! this is the beginning of a long do loop
c       nepo is the total number of E value in the grid
       write (6,*) ie,jmax(ie)
      j = 0 
 120  continue
      ert = bt * dble(j*(j+1)) ! rotational energies of ts and react
      err = br * dble(j*(j+1))
      sum = 0.d0
      do i = -ist+1,nep ! nep  = nepo + nv0 
         eactt = en(i) - tsen - ert ! ?
         eactr = en(i) - ren - err
         denst(i) = 0.d0
         densr(i) = 0.d0
         if (eactt .gt. 0.d0) denst(i) = dexp(rst*dlog(eactt) + rct) 
         if (eactr .gt. 0.d0) densr(i) = dexp(rsr*dlog(eactr) + rcr) 
      enddo
c
c     convolution of rovibrational densities of states for TS and reactants
c
      do i = 1,nfrt ! loop over freq TS
         nind = nint(freqt(i)/de) ! ist?
             ind = int((tsen + ert + freqt(i) - en(-ist+1)) / de) + 1 - ist  
             if (ind .le. nep) then
                do ii = 1 + ind, nep, 1
                   enc = en(ii) - freqt(i)
                   ind1 = int((enc - en(-ist+1)) / de) + 1 - ist 
                   if (ind1 .ge. 1-ist) denst(ii) = denst(ii) + denst(ind1) 
                enddo
             endif
          enddo
          do i = 1,nfrr ! react
             nind = nint(freqr(i)/de)
             ind = int((ren + err + freqr(i) - en(-ist+1)) / de) + 1 - ist 
             if (ind .le. nep) then
                do ii = 1 + ind, nep, 1
                   enc = en(ii) - freqr(i)
                   ind1 = int((enc - en(-ist+1)) / de) + 1 - ist 
                   if (ind1 .ge. 1-ist) densr(ii) = densr(ii) + densr(ind1) 
                enddo
             endif
          enddo
c
c         rate constants 
c
             prob1 = 0.d0
             prob2 = 0.d0
             ic = -ist+1
             do while (ic .lt. ie+nv0)
                dpr2 = 0.d0
    c               If 
                if (en(ie) .ge. en(ic)) dpr2 = 1.d0
                v0mod = v0 + ert - err
            if (bortm .ne. 0.d0 .and. en(ie)+v0mod .ge. en(ic)) then 
c               Call eck (see below) in order to get tunnelling prob
               dpr1 = eck (en(ie)-en(ic)+v0mod, v0mod, bortm) 
            else
               dpr1 = dpr2
            endif 
            prob1 = prob1 + dpr1 * denst(ic) * de
            prob2 = prob2 + dpr2 * denst(ic) * de 
            ic = ic + 1
         enddo 
         const1(ie) = const1(ie) + (2.d0*j+1.d0)*prob1 
         const2(ie) = const2(ie) + (2.d0*j+1.d0)*prob2 
      denstot(ie) = denstot(ie) + (2.d0*j+1.d0)*densr(ie)
      j = j + 1
      if (j .le. jmax(ie)) goto 120
      enddo
      open (2, file=namout,status='unknown')
      do i = 1, nepo
         write (2,*) en(i), denstot(i)                ! write densities 
         const1(i) = const1(i) / (pcon * denstot(i))
         const2(i) = const2(i) / (pcon * denstot(i))
c         write (2,*) en(i), const1(i), const2(i)        ! write k(e)
      enddo
      close (2)
      end 
      double precision function dlgam(xx)
c
c     log of the gamma function for xx > 0
c     taken from NUMERICAL RECIPES
c
      implicit double precision (a-h,o-z)
      dimension cof(6)
      save cof,stp
      data cof,stp/76.18009172947146d0, -86.50532032941677d0, 
     +  24.01409824083091d0, -1.231739572450155d0, 
     +  0.1208650973866179d-02, 
     +  -0.5395239384953d-05, 2.5066282746310005d0/
      x = xx
      y = x
      tmp = x + 5.5d0
      tmp = (x + 0.5d0)*dlog(tmp) - tmp
      ser = 1.000000000190015d0
      do j = 1,6
         y = y + 1.d0
         ser = ser + cof(j) / y
      enddo
      dlgam = tmp + dlog(stp * ser / x)   
      return
      end
      double precision function eck (e, v0, bortm)
c
c     probability of transmission through an Eckart barrier
c     of height v0 and beta/sqrt(mass) = bortm at energy e
c
      implicit double precision (a-h,o-z)
      parameter (pcon = 6.62606896d-34)
      pi = dacos (-1.d0)
      hbar = pcon / (2.d0 * pi)
      val1 = 4.d0 * pi * dsqrt(2.d0 * e) / (hbar * bortm)
      val2 = 8.d0*v0/((hbar*bortm)**2) - 0.25d0
      if (val2 .lt. 0.d0) then
         dc = dcos (2.d0 * pi * dsqrt(-val2)) * sech(val1)
      else if (val1 .ge. val2) then
         val2 = 2.d0 * pi * dsqrt(val2)
         dc = (dexp(val2 - val1) + dexp(-val2-val1))/
     +        (1.d0 + dexp(-2.d0*val1))
      else
         val2 = 2.d0 * pi * dsqrt(val2)
         dc = (1.d0 + dexp(-2.d0*val2))/
     +        (dexp(val1 - val2) + dexp(-val1-val2))
      endif
      eck = (1.d0 - sech(val1))/(1.d0 + dc)
      return
      end
      double precision function sech(x)
c
c     hyperbolic secant function
c
      implicit double precision (a-h,o-z)
      sech = 2.d0 / (dexp(x) + dexp(-x))
      return
      end
