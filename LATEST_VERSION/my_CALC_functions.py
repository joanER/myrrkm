import numpy as np
import math, time
from numba import jit, objmode
import numba
import matplotlib.pylab as plt
#######################################
# NOTES
# har_to_cm1  = 219474.63
# J_to_cm1    = 5.03445e22
# kJmol_to_cm1= 83.593 
# har_to_J    = 43.60e-19
#######################################
#
#
######### Physical constants #################################################################################
#
def phys_constants(string,printing=False):
    '''
    Returns the following constatns:
    - (NA) > Avogadro's numb in mol-1
    - (c)  > Light speed in cm/s
    - (h)  > Planck's ctnt in Js
    - (kB) > Boltzmann's ctnt in J/K
    '''
    listNA = ['NA','na','Na','Avogadro','avogadro','avo','Avo']
    listLightSpeed = ['c','light speed','light velocity']
    listh  = ['h', 'planck', 'Planck']
    listkb = ['kb','kB','k','boltzmann','Boltzmann']
    if string in listNA:
        if printing==True: print(string,'=',6.02214076e23,'mol-1')
        return 6.02214076e23 # mol-1, from NIST [https://physics.nist.gov/cgi-bin/cuu/Value?na|search_for=avogadro]
    if string in listLightSpeed:
        if printing==True: print(string,'=',29979245800.,'cm s-1')
        return 29979245800.0 # cm/s, from NIST [https://physics.nist.gov/cgi-bin/cuu/Value?c|search_for=light+speed]
    if string in listh:
        if printing==True: print(string,'=',6.62607015e-34,'Js')
        return 6.62607015e-34 # J*s, from NIST [https://physics.nist.gov/cgi-bin/cuu/Value?h|search_for=planck+constant]
    if string in listkb:
        if printing==True: print(string,'=',1.380649e10-23,'J K-1')
        return 1.380649e-23 #J/K, from NIST [https://physics.nist.gov/cgi-bin/cuu/Value?k|search_for=boltzmann+constant]
#
########## Convert units ###################################################################################
#
def convert_Eunits(unit_from,unit_to):
    '''
    Converts between har, J, kJ/mol, kcal/mol, eV, K, 1/cm, GHz
    '''
    listkJmol = ['kJ/mol', 'kjmol', 'kJmol', 'kJ mol-1', 'kjmol-1', 'kj mol-1']
    listhar   = ['har','Hartree', 'Eh']
    listJoule = ['J','j','Joule','joule']
    listcm1   = ['cm-1','1/cm','cm1']
    listeV    = ['ev','eV','electonvolt','electonvolts']
    listkcalmol=['kcal/mol', 'kcalmol', 'kcalmol', 'kcal mol-1', 'kcalmol-1', 'kcal mol-1']
    listK     = ['K','k','kelvin','Kelvin']
    listGHz   = ['ghz','GHZ','GHz','gigahertz','giga hertz','Giga Hertz']
    if unit_from == unit_to:
        return 1.0
    elif unit_from in listkJmol:
        if unit_to in listhar:     return  1.0/convert_Eunits('har','kJ/mol')
        elif unit_to in listJoule: return  1000.0/phys_constants('NA')
        elif unit_to in listcm1:   return  convert_Eunits('kJ/mol','J') * convert_Eunits('J','cm-1') 
        elif unit_to in listeV:    return  convert_Eunits('kJ/mol','J') * convert_Eunits('J','eV')
        elif unit_to in listkcalmol:return 1.0/4.184
        elif unit_to in listK:     return  convert_Eunits('kJ/mol','J') * convert_Eunits('J','K') 
        elif unit_to in listGHz:   return  convert_Eunits('kJmol','J') * convert_Eunits('J','GHz')
        else: print('unit_to = {} not among the possibilities: har, J, kJ/mol, kcal/mol, eV, K, 1/cm, GHz'.format(unit_to))
    elif unit_from in listhar:
        if unit_to in listkJmol:   return  2625.5002
        elif unit_to in listJoule: return  convert_Eunits('har','kJ/mol') * convert_Eunits('kJ/mol','J') 
        elif unit_to in listcm1:   return  convert_Eunits('har','J') * convert_Eunits('J','cm1') 
        elif unit_to in listeV:    return  27.211399
        elif unit_to in listkcalmol:return convert_Eunits('har','kJ/mol') * convert_Eunits('kJ/mol','kcal/mol')
        elif unit_to in listK:     return  convert_Eunits('har','J') * convert_Eunits('J','K') 
        elif unit_to in listGHz:   return  convert_Eunits('har','J') * convert_Eunits('J','GHz') 
        else: print('unit_to = {} not among the possibilities: har, J, kJ/mol, kcal/mol, eV, K, 1/cm, GHz'.format(unit_to))
    elif unit_from in listJoule:
        if unit_to in listhar:     return   1.0 / convert_Eunits('har','J')
        elif unit_to in listkJmol: return   phys_constants('NA')/1000 
        elif unit_to in listcm1:   return   1.0/phys_constants('h')/phys_constants('c')
        elif unit_to in listeV:    return   convert_Eunits('J','har') * convert_Eunits('har','eV') 
        elif unit_to in listkcalmol:return  phys_constants('NA')/1000/4.184
        elif unit_to in listK:     return   1.0/phys_constants('kB')
        elif unit_to in listGHz:   return   1.0/convert_Eunits('GHz','J')
        else: print('unit_to = {} not among the possibilities: har, J, kJ/mol, kcal/mol, eV, K, 1/cm, GHz'.format(unit_to))
    elif unit_from in listcm1:
        if unit_to in listhar:     return  1.0/convert_Eunits('har','cm1')
        elif unit_to in listJoule: return  phys_constants('h')*phys_constants('c')
        elif unit_to in listkJmol: return  convert_Eunits('cm1','J') * convert_Eunits('J','kJ/mol')
        elif unit_to in listeV:    return  convert_Eunits('cm1','J') * convert_Eunits('J','eV') 
        elif unit_to in listkcalmol:return convert_Eunits('cm1','J') * convert_Eunits('J','kcal/mol') 
        elif unit_to in listK:     return  convert_Eunits('cm1','J') * convert_Eunits('J','K')
        elif unit_to in listGHz:   return  convert_Eunits('cm1','J') * convert_Eunits('J','GHz')
        else: print('unit_to = {} not among the possibilities: har, J, kJ/mol, kcal/mol, eV, K, 1/cm, GHz'.format(unit_to))
    elif unit_from in listeV:
        if unit_to in listhar:     return  1.0/convert_Eunits('har','eV')
        elif unit_to in listJoule: return  1.0/convert_Eunits('J','eV') 
        elif unit_to in listkJmol: return  1.0/convert_Eunits('J','eV') * convert_Eunits('J','kJ/mol')
        elif unit_to in listcm1:   return  1.0/convert_Eunits('J','eV') * convert_Eunits('J','cm1')
        elif unit_to in listkcalmol:return 1.0/convert_Eunits('J','eV') * convert_Eunits('J','kcal/mol')
        elif unit_to in listK:     return  1.0/convert_Eunits('J','eV') * convert_Eunits('J','K')
        elif unit_to in listGHz:   return  convert_Eunits('eV','J') * convert_Eunits('J','GHz')
        else: print('unit_to = {} not among the possibilities: har, J, kJ/mol, kcal/mol, eV, K, 1/cm, GHz'.format(unit_to))
    elif unit_from in listkcalmol:
        if unit_to in listhar:     return  1.0/convert_Eunits('har','kcal/mol')
        elif unit_to in listJoule: return  1.0/convert_Eunits('J','kcal/mol')
        elif unit_to in listkJmol: return  1.0/convert_Eunits('kJ/mol','kcal/mol')
        elif unit_to in listcm1:   return  1.0/convert_Eunits('cm1','kcal/mol')
        elif unit_to in listeV:    return  1.0/convert_Eunits('eV','kcal/mol')
        elif unit_to in listK:     return  convert_Eunits('kcal/mol','J') * convert_Eunits('J','K')
        elif unit_to in listGHz:   return  convert_Eunits('kcal/mol','J') * convert_Eunits('J','GHz')
        else: print('unit_to = {} not among the possibilities: har, J, kJ/mol, kcal/mol, eV, K, 1/cm, GHz'.format(unit_to))
    elif unit_from in listK:
        if unit_to in listhar:     return  1.0/convert_Eunits('har','K')
        elif unit_to in listJoule: return  1.0/convert_Eunits('J','K')
        elif unit_to in listkJmol: return  1.0/convert_Eunits('kJ/mol','K')
        elif unit_to in listcm1:   return  1.0/convert_Eunits('cm1','K')
        elif unit_to in listkcalmol:return 1.0/convert_Eunits('kcal/mol','K')
        elif unit_to in listeV:    return  1.0/convert_Eunits('eV','K')
        elif unit_to in listGHz:   return  convert_Eunits('K','J') * convert_Eunits('J','GHz')
        else: print('unit_to = {} not among the possibilities: har, J, kJ/mol, kcal/mol, eV, K, 1/cm, GHz'.format(unit_to))
    elif unit_from in listGHz:
        if unit_to in listhar:     return  1.0/convert_Eunits('har','GHz')
        elif unit_to in listJoule: return  1.0e9 * phys_constants('h')
        elif unit_to in listkJmol: return  1.0/convert_Eunits('kJ/mol','GHz')
        elif unit_to in listcm1:   return  1.0/convert_Eunits('cm1','GHz')
        elif unit_to in listkcalmol:return 1.0/convert_Eunits('kcal/mol','GHz')
        elif unit_to in listeV:    return  1.0/convert_Eunits('eV','GHz')
        elif unit_to in listK:     return  1.0/convert_Eunits('K','GHz')
        else: print('unit_to = {} not among the possibilities: har, J, kJ/mol, kcal/mol, eV, K, 1/cm, GHz'.format(unit_to))
        
        
    else: print('unit_from = {} not among the possibilities: har, J, kJ/mol, kcal/mol, eV, K, 1/cm, GHz'.format(unit_from))
#
##############################################################################################################
#
har_to_cm1  = convert_Eunits('har','cm1') # 219474.63
J_to_cm1    = convert_Eunits('J','cm1')   # 5.03445e22
kJmol_to_cm1= convert_Eunits('kJ/mol','cm1') # 83.593 
har_to_J    = convert_Eunits('har','J')   # 43.60e-19
#
def SimpleEyring(Eact,T,units_from='cm1'):
    "Eact in desired units; returns k s-1 (the function converts the barrier from unit_from to K internally"
    return phys_constants('kB')*T/phys_constants('h') * np.exp(-Eact*convert_Eunits(units_from,'K')/T)
#
def SimpleEyringArray(Eact,units_from='cm1'):
    "Eact in desired units; returns k s-1 (the function converts the barrier from unit_from to K internally"
    maxT = globals()["GLOBALS"]["maxT"]
    return np.array([SimpleEyring(Eact,i+1,units_from=units_from) for i in range(maxT)],dtype=np.float64)
#
def PartFunct_Vib(freq_array,T,units_freq="cm1"):
    return np.prod(1. / (1. - np.exp(- freq_array * convert_Eunits(units_freq,"K") / T)))
def PartFunct_Rot(RotCts_array,T,sim=1.0):
    kB = phys_constants('kB')
    h  = phys_constants('h')
    return np.sqrt(np.pi) / sim * (kB*T/np.pi/h)**(3.0/2.0) * 1.0/np.sqrt(np.prod(RotCts_array*1.0e9) )

def FullEyring_DensStates(E_grid,Eact,dens_rg,dens_ts,units_from='cm1',analytical_pf=False,vib_rg=np.array([0],dtype=np.float64),vib_ts=np.array([0],dtype=np.float64),rot_rg=np.array([0],dtype=np.float64),rot_ts=np.array([0],dtype=np.float64)):
    """
    Returns an array of size maxT, k in s-1. 
    Eact in desired units (the function converts the barrier from unit_from to K internally
    If analytical_pf==True, then the analytical formulas are used, and the vib/rots of the stat pts need to be added.
    """
    rot_flag = globals()["GLOBALS"]["ROT_Dens"]
    maxT = globals()["GLOBALS"]["maxT"]
    kB   = phys_constants('kB')
    h    = phys_constants('h')
    
    beta  = lambda x: 1/(kB*convert_Eunits('J','cm1') * x)
    kB_over_h = kB/h
    
    # T-dependent part. functions (arrays)
    if analytical_pf==False:
        PFrg  = np.array(calculate_part_function(E_grid,dens_rg),dtype=np.float64)
        PFts  = np.array(calculate_part_function(E_grid,dens_ts),dtype=np.float64)
    else:
        PFrg = np.array([0]*maxT,dtype=np.float64)
        PFts = np.array([0]*maxT,dtype=np.float64)
        if rot_flag== False:
            for t in range(maxT):
                t=t+1
                PFrg[t-1] = PartFunct_Vib(vib_rg,t)
                PFts[t-1] = PartFunct_Vib(vib_ts[1:],t)    
        else:
            for t in range(maxT):
                t=t+1
                PFrg[t-1] = PartFunct_Vib(vib_rg,t)*PartFunct_Rot(rot_rg,t)
                PFts[t-1] = PartFunct_Vib(vib_ts[1:],t)*PartFunct_Rot(rot_rg,t)    

    rate_constant = np.zeros(maxT,dtype=np.float64)
    for index in range(maxT):
        T = float(index+1.0)
        rate_constant[index] = PFts[index]/PFrg[index] * kB_over_h*T * np.exp(-Eact*beta(T))
    return rate_constant
#
##############################################################################################################
#
@jit(nopython=True)
def round_numb(n):
    return int(np.rint(n))
#     # takes a value and rounds it up/down properly, just like nint would do in Fortran
#     if np.abs(n - np.floor(n)) < 0.5:
#         return int(np.floor(n))
#     else:
#         return int(np.ceil(n))
def find_bin(value,grain,E_grid):
    index = round_numb(value/grain)
    if value <= E_grid[index]:
        return index
    else:
        return index+1
def make_Egrid(Ein,dE,nEpts_TOT):
    return np.array([Ein + i*dE for i in range(nEpts_TOT)],dtype=np.float64)
def Erot(j,rot_cts,Ndof=3):
    '''
    rot_cts in cm-1
    Ndof    = nr of degrees of freedom, can be 3 or 2
    For Ndof>2 I expect an array/list of 3 rot cts
    '''
    if Ndof == 3:
        b = (rot_cts[0]*rot_cts[1]*rot_cts[2])**1./3.
        deg = (2.*J+1.)**2.
        return b * j*(j+1.)
    elif Ndof == 2:
        b = rot_cts
        deg = 2.*J+1.
        return b * j*(j+1.)

#
##############################################################################################################
from scipy.special import gamma as GAMMA
def rot_dens(E_grid,e_thresh,RotCts,sigma=1.):
    """Input:
    rot cts in GHz
    e, e_thresh in cm-1
    Out in 1/cm-1"""
    t0 = time.time()
    dens = np.zeros(len(E_grid),dtype=np.float64) # initialize array
    A = RotCts[0]*convert_Eunits('GHz','cm1') # GHz to cm-1
    B = RotCts[1]*convert_Eunits('GHz','cm1') # GHz to cm-1
    C = RotCts[2]*convert_Eunits('GHz','cm1') # GHz to cm-1
    DoF = float(len(RotCts))
    for ie,e in enumerate(E_grid):
        if e > e_thresh:
            dens[ie] = (e-e_thresh)**(DoF/2. - 1.) * GAMMA(DoF)/sigma/(A*B*C)**(DoF/6.0)
            #(e-e_thresh)**(DoF/2. - 1.) * GAMMA(DoF)/sigma/(A*B*C)**(DoF/2.0*1.0/3.0)
        else:
            dens[ie] = 0.0
    if globals()["GLOBALS"]["Printing_level"]=='D': print(" >> Rot dens took: {:.2f} s".format(time.time()-t0))
    return dens
def rot_sum(E_grid,e_thresh,RotCts,sigma=1.):
    dens_array = rot_dens(E_grid,e_thresh,RotCts,sigma=1.)
    de = E_grid[1]-E_grid[0]
    sum_rot = np.zeros(len(E_grid),dtype=np.float64)
    for ind_E, ener in enumerate(E_grid):
        if ind_E!=0:
            sum_rot[ind_E] = sum_rot[ind_E-1] + dens_array[ind_E]*de
        else: sum_rot[ind_E] = dens_array[ind_E]*de
    return sum_rot


#
##############################################################################################################
@jit(nopython=True)
def BeyerSwinehart(E_grid,NormModes,E_StatPoint,nEpts,dens=np.array([0])):
    #Input Energies in cm-1
    #  dE = energy grain
    #  E_StatPoint = energy of the stationary point (PES minimum/TS), must contain ZPE
    #  All energies must be set wrt Erg (the zero of energies starts at the ZPE-coorected reagents energy)
    #  nEpts covers the globals nEpts + those required by the system (dependent on the barrier size)
    #  dens default is [-1], if this happens the function automatically sets it to [1,0,0,0,0...] as it should be
    #   if there is no rotational dens
    #RETURNS:
    #    E_grid -- in units of cm-1
    #    dens   -- in (cm-1)-1
    
    with objmode(dE='f8',t0='f8',pIn2Min='i4'): #Ein='f8',Emin='f8'
        dE   = globals()["GLOBALS"]['dE']
        Ein  = globals()["GLOBALS"]['Ein']
        Emin = globals()["GLOBALS"]['Emin']
        t0 = time.time()
        pIn2Min = round_numb((Ein - Emin) / dE)
        #---+---+
    if dens.all() == 0:
        # Initialize density of states by putting 1 state on the lowest E
        #  possible (Erg for reagents, Ets for TS). 
        # Examlpe: for reagents the 1st element will be 1.0, the rest 0s,
        #  for the TS this will be an array of zeros except at the energy bin
        #  of the energy barrier, which will be 1.0
        dens = np.zeros(nEpts,dtype=np.float64)
        dens[round_numb(E_StatPoint/dE)] = 1.0
    #---+---+
    for iv,FREQ in enumerate(NormModes[:]):
        #Ept = find_bin( (E_StatPoint+FREQ-E_grid[-pIn2Min+1]),dE,E_grid) + 1-pIn2Min # energy point
        Ept = round_numb( (E_StatPoint+FREQ-E_grid[-pIn2Min+1])/dE) + 1-pIn2Min # energy point
        if Ept <= nEpts:
            for k in range(Ept,nEpts):
                #index = find_bin( (E_grid[k]-FREQ-E_grid[-pIn2Min+1]),dE,E_grid) + 1-pIn2Min
                index = round_numb( (E_grid[k]-FREQ-E_grid[-pIn2Min+1])/dE) + 1-pIn2Min
                if index >= 1-pIn2Min:
                    dens[k]+=dens[index]
    with objmode():
        if globals()["GLOBALS"]["Printing_level"]=='D':
            print(" >> Beyer-Swinehart algorithm took: {:.2f} s".format(time.time()-t0))
    return dens
#
##############################################################################################################
#
#@jit(nopython=True)
def T_Eck_unsym_BaerHase1996(Et,v0,v1,frq): ## Et is measured from the top of the barrier
    # All in SI
    frq=np.abs(frq)
    with objmode(h='f8'):
        h=phys_constants('h')
    hfrq = h*frq
    _2pi = 2.0*np.pi
    # hbar = h/2.0/np.pi # Js
    term1 = 2.*_2pi/hfrq / (1./np.sqrt(v1) + 1./np.sqrt(v1))
    a = np.sqrt(Et + v0) * term1
    b = np.sqrt(Et + v1) * term1
    c = _2pi * np.sqrt( v0*v1/(hfrq)**2. - 1./16 )
    #
    numer = np.sinh(a)*np.sinh(b)
    denom = np.sinh((a + b)/2.0)**2.0 + np.cosh( c )**2 
    return numer/denom
#
##############################################################################################################
#
@jit(nopython=True)
def T_Eck_unsym(E,v0,v1,frq): ## I am using this one, defined from the bottom (Johnston & Heicklen 1961)
    frq = np.abs(frq)
    # All in SI
    with objmode(h='f8'):
        h = phys_constants('h')
    hbar = h/2.0/np.pi # Js
    alph1 = v0/(hbar*frq)
    alph2 = v1/(hbar*frq)
    zeta  = E /v0
    term1 = (4*np.pi / (h*frq)) / ( 1.0/np.sqrt(v1) + 1.0/np.sqrt(v0) )
    a = term1 * np.sqrt(E)
    b = term1 * np.sqrt(E-v0+v1)
    numer = np.cosh(a + b) - np.cosh(a - b) 
    if v0*v1/(h*frq)**2 >= 1.0/16: # no imag numbs 
        denom = np.cosh( (a + b) ) + np.cosh( 4*np.pi* np.sqrt(v0*v1/(h*frq)**2 - 1.0/16) ) 
    else:
        denom = np.cosh( (a + b) ) + np.cos( 4*np.pi* np.sqrt(v0*v1/(h*frq)**2 - 1.0/16) ) 
    return numer/denom
#
##############################################################################################################
#
@jit(nopython=True)
def T_Eck_unsym_old(E,v0,v1,frq): ## I am using this one
    # All in SI
    hbar  = 6.62607015e-34/2/np.pi # Js
    alph1 = v0/(hbar*frq)
    alph2 = v1/(hbar*frq)
    zeta  = E/v0
    term1 = 1 / ( 1/np.sqrt(alph1) + 1/np.sqrt(alph2) )
    a = 2 * np.sqrt(alph1*zeta) * term1
    b = 2 * np.sqrt( (zeta-1)*alph1 + alph2 ) * term1
    numer = np.cosh(a + b) - np.cosh(a - b) 
    denom = np.cosh(a + b) + np.cosh( np.sqrt(4.0*alph1*alph2 - np.pi**2) ) 
    return numer/denom
#
##############################################################################################################
#
#@jit(nopython=True)
def T_Eck_unsym2(Et,v0,v1,frq):
    frq = np.abs(frq)
    # All in SI
    # E is defined from the barrier
    # From Baer and Hase 1996 pg 266 (section 7.6.1)
    with objmode(h='f8'):
        h = phys_constants('h') #6.62607015e-34 # Js
    #
    piece1 = 4.*np.pi/h/frq
    piece2 = 1./(1./np.sqrt(v0)+1./np.sqrt(v0))
    a =  piece1*np.sqrt(Et+v0)*piece2
    b =  piece1*np.sqrt(Et+v1)*piece2
    c = 2.*np.pi*np.sqrt( v0*v1/(h*frq)**2.0 - 1./16.)
    numer = np.sinh(a)*np.sinh(b)
    denom = np.sinh(a/2.+b/2.)**2. + np.cosh(c)**2.
    return numer/denom
#
##############################################################################################################
#
@jit(nopython=True,parallel = True)
def Sum_States(E_grid,dens,barrier_E,bkg_barrier_E=-1,tn=False,ts_frq=0):
    '''
    E_grid: in cm-1
    dens:   in (cm-1)-1
    other energies in cm-1 too
    '''
    with objmode(t0='f8',c='f8',h='f8',dE='f8'):#NA='f8',kJmol_to_cm1='f8',Ein='f8',Emin='f8'
        t0  = time.time()
        c   = phys_constants('c') # 29979245800 # cm/s
        h   = phys_constants('h') # 6.62607004e-34 # Js
        dE  = globals()["GLOBALS"]["dE"]
        Ein = globals()["GLOBALS"]["Ein"]
        Emin= globals()["GLOBALS"]["Emin"]
        
        #NA  = phys_constants('NA') # 6.022e23 # mol-1
        #kJmol_to_cm1 = convert_Eunits('kJ/mol','cm1') # 83.59345392546533 
    c_h = c*h # 1.9864458241717584e-23 # J*cm
    #
    if bkg_barrier_E==-1:
        bkg_barrier_E = barrier_E # if no bkg barrier specified, take it to be symmetric
    if tn==True:
        SumStates_TN =  np.zeros(len(dens),dtype=np.float64)
        ts_frq = np.abs(ts_frq)*c
        barr_joules     = barrier_E    * c_h 
        bkg_barr_joules = bkg_barrier_E* c_h 
        egrid_joules    = E_grid[:]    * c_h
    #
    SumStates =  np.zeros(len(dens),dtype=np.float64)
    for en_pt,en in enumerate(E_grid):
        PR_CL = 0.0
        PR_TN = 0.0
        # Integrate
        initial =      -round_numb(barrier_E/dE)
        final   = en_pt-round_numb(barrier_E/dE)
        for index in np.arange(initial,final,1):
            dPR_CL = 0.0
            dPR_TN = 0.0
            if index >= 0:
                dPR_CL = 1.0
            if tn==True:
                if E_grid[index+round_numb(barrier_E/dE)] <= barrier_E and np.abs(E_grid[index+round_numb(barrier_E/dE)]) >= np.abs(bkg_barrier_E-barrier_E):
                    dPR_TN=T_Eck_unsym_old(egrid_joules[index+round_numb(barrier_E/dE)],
                                           v0=barr_joules,v1=bkg_barr_joules,frq=ts_frq)
                else:
                    dPR_TN = dPR_CL
            PR_CL += dPR_CL * dens[en_pt-index] * dE # Summation
            if tn==True:
                PR_TN += dPR_TN * dens[en_pt-index] * dE # summation
        SumStates[en_pt]=PR_CL
        if tn==True:
            SumStates_TN[en_pt]=PR_TN
    if tn==True:
        with objmode():
            if globals()["GLOBALS"]["Printing_level"]=='D':
                print(" >> Numerical sum of states took w/ tunn: {:.2f} s".format(time.time()-t0))
        return SumStates_TN
    else:
        with objmode():
            if globals()["GLOBALS"]["Printing_level"]=='D':
                print(" >> Numerical sum of states took: {:.2f} s".format(time.time()-t0))
        return SumStates
#
#
##############################################################################################################
#
@jit(nopython=True)
def micocan_kE(N,p):
    '''
    p = reactant's density of states (1/cm-1)
    N = ts's sum of states
    '''
    with objmode(h='f8',J_to_cm1='f8',nEpts='i4'):
        h = phys_constants('h')
        J_to_cm1 = convert_Eunits('J','cm1')
        nEpts=globals()["GLOBALS"]['nEpts']
    kE = np.zeros(nEpts,dtype=np.float64)
    #
    for i in range(nEpts):
        kE[i] = N[i]/p[i] # cm-1
    return kE/(h*J_to_cm1) # convert k(E) into s-1
#
def micocan_kE_(N,p,nEpts): ## //!\\ THIS IS A FAKE FUNCTION
    '''
    p = reactant's density of states (1/cm-1)
    N = ts's sum of states
    '''
    kE = np.zeros(nEpts,dtype=np.float64)
    with objmode(h='f8',J_to_cm1='f8'):
        h = phys_constants('h')#6.62607004e-34 # Js
        J_to_cm1 = convert_Eunits('J','cm1') #5.03445e22
    #
    for i in range(nEpts):
        if p[i]>0:
            kE[i] = N[i]/p[i] # cm-1
        else:  kE[i] = kE[i-1]
    return kE/(h*J_to_cm1) # convert k(E) into s-1
#
########## Q(T) ##############################################################################################
#
@jit(nopython=True,parallel=True)
def calculate_part_function(E_grid,dens,E_Stat=0.0):
    '''
    E_grid: in cm-1
    E_Stat: energy of the stationary point in cm-1
    T in K (taken from globals)
    dens in 1/cm-1
    Returns PF in dens units.
    '''
    with objmode(t0='f8',kB='f8',maxT='i4',dE='f8'): #nEpts='i4'
        t0 = time.time()
        kB = phys_constants('kB') * convert_Eunits('J','cm1') # Boltzmann ctn in cm-1/K
        maxT  = globals()["GLOBALS"]["maxT"]
        #nEpts = globals()["GLOBALS"]['nEpts']
        dE    = globals()["GLOBALS"]['dE']
    beta       = lambda x: 1.0/(kB * x) # Calculate Beta                              
    PF         = np.zeros(maxT,dtype=np.float64)       # Initiatlize part fn array of T
    for it, T in enumerate(range(maxT)):
        T += 1.0 # Starts at zero, need to correct for that
        init_e = round_numb(E_Stat/dE)
        for ie,e in enumerate(E_grid[init_e:]): # compute partition function
            #dd=dens[ie+init_e]
            #ee=np.exp(-e * beta(T))
            #PF[it] += dd*ee
            PF[it] += dens[ie+init_e] * np.exp(-e * beta(T))
    with objmode():
        if globals()["GLOBALS"]["Printing_level"]=='D':
            print(" >> Numerical partition function took: {:.2f} s".format(time.time()-t0))
    return PF
#
########## k(T) ##############################################################################################
#
@jit(nopython=True,parallel=True)
def calcuate_rate_T(E_grid,kE,dens,PF): # <==> boltzmann summation
    """
    Egrid in cm-1
    kE    in 1/s
    dens  in 1/cm-1
    maxT  in K
    PF    in 
    """
    with objmode(t0='f8',kB='f8',maxT='i4',nEpts='i4'):
        t0 = time.time()
        kB = phys_constants('kb')*convert_Eunits('J','cm1') # 8.314462e-3*83.59345392546533 # Boltzmann ctn in cm-1/K    
        maxT = globals()["GLOBALS"]["maxT"]
        nEpts= globals()["GLOBALS"]["nEpts"]
    beta  = lambda x: 1/(kB * x)          # Calculate Beta                             
    r_ctn = np.zeros(maxT,dtype=np.float64)                # Initiatlize k(T) array of T
    for it, T in enumerate(range(maxT)):
        T += 1 # it starts at zero, need to correct for that   
        for ie,e in enumerate(E_grid[:nEpts]):
            #r_eck[it] = r_eck[it] + (self.microcan_DF['kE_Eck'][ie] *
            #                         self.microcan_DF['totDENS'][ie] *
            #                         np.exp(-e / kJmol_to_J * beta(_T))) / PF[it]
            #if e*beta(_T) < 600: 
            r_ctn[it] += (kE[ie] * dens[ie] * np.exp( -e * beta(T))) / PF[it]
    with objmode():
        if globals()["GLOBALS"]["Printing_level"]=='D': print(" >> Boltzmann averaging to get k(T) took: {:.2f} s".format(time.time()-t0))
    return r_ctn
#
########## Sanity check ####################################################################################
#
# Function to check rho * exp(-E/maxT)
def SanityCheck(maxT,STEPS):
    """
    Input: maxT is an integer, STEPS is either a Dictionary or a list of dictionaries, nEpts is an int
    Output: a plot that must grow and decrease within the Egrid of
            the STEP: step['dens_rg'][ie] * np.exp(-e/kJmol_to_J * beta(maxT))
    """
    nEpts = globals()["GLOBALS"]["nEpts"]
    if not isinstance(STEPS,list):
        if isinstance(STEPS,dict):
            lenSTEPS = 1
            STEPS = [STEPS]
        else: print("STEPS: Input a dictionary or a list of dictionaries")
    else: lenSTEPS = len(STEPS)
    #
    fig, axs = plt.subplots(nrows=1, ncols=lenSTEPS,figsize=(5*lenSTEPS,4))
    if lenSTEPS==1: axs = [axs]
    #
    for STEP,ax in zip(STEPS,axs):
        y=[0 for i in STEP['E_grid']]
        for ie,e in enumerate(STEP['E_grid']):
            y[ie]= STEP['dens_rg'][ie]/convert_Eunits('cm1','J') * np.exp(-e*convert_Eunits('cm1','K')/maxT)
        x = [i*convert_Eunits('cm1','J') for i in STEP['E_grid']]
        #y=[ ts.microcan_DF['totDENS'][ie] * np.exp(-ts.microcan_DF['E'][ie]/kJmol_to_J * beta(maxT)) for ie in E_ind]
        ax.plot(x[:nEpts],y[:nEpts],label=STEP['name'])
    for ax in axs: 
        ax.legend()
        ax.set_xlabel('E (J)')
        ax.set_ylabel(r'$\rho(E) \times e^{- E*\beta(T_{max})}$ [1/J]')
        ax.set_title('Sanity Check')
    plt.tight_layout()
    plt.show()
