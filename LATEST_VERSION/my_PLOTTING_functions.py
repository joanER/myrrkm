import matplotlib.pylab as plt
import numpy as np
import warnings
warnings.filterwarnings("error")

def plot_rho_and_N(E_grid,list_dens,list_names_dens,list_sums,list_names_sums,nEpts,Xaxis_label=r"Energy [$J$]",conv_units='cm1_to_J',Sk_tot_dens=-1,mask_ts=None):
#def plot_rho_and_N(E_grid,dens_rg,dens_ts,list_sums,list_names_sums,Xaxis_label=r"Energy [$J$]"):
    '''
    E_grid in J (or unitless)
    densities in J-1
    '''
    cm = plt.get_cmap('gnuplot')# gist_stern
    LINE_STYLES = ['solid', 'dashed', 'dotted', 'dashdot']
    NUM_STYLES, NUM_C_dens, NUM_C_sums = len(LINE_STYLES), len(list_dens), len(list_sums)
    J_to_cm1    = 5.03445e22
    #
    if Sk_tot_dens!=-1: # if you want to read a tot_dens file by Skouteris, then the data will be loaded here
        with open(Sk_tot_dens,'r') as totdens_file:
            dens_Data = totdens_file.readlines()
        edens_sk = [0]*len(dens_Data)
        dens_sk  = [0]*len(dens_Data)
        for i,line in enumerate(dens_Data):
            edens_sk[i] = float(line.split()[0])
            dens_sk[i]  = float(line.split()[1])
    #
    plt.rcParams.update({'font.size': 12})
    fig, ax = plt.subplots(1,2,figsize=(12,5))
    #
    if conv_units=='cm1_to_J':
        for i,el in enumerate(list_dens): list_dens[i]=el*J_to_cm1
    #
    for i,(y,lbl) in enumerate(zip(list_dens,list_names_dens)):
        l = ax[0].plot(E_grid[:nEpts],list_dens[i][:nEpts],label=lbl)
        l[0].set_color(cm(i/NUM_C_dens))
        l[0].set_linestyle(LINE_STYLES[i])
    ax[0].set_ylabel(r"Density of states [J$^{-1}$]")
    #
    # plot skotueri's data if avail
    if Sk_tot_dens!=-1:
        fig2,axx = plt.subplots(1,1,figsize=(6,6))
        ax[0].plot(edens_sk,dens_sk,label="tot_dens_sk",c='orange')
        #axx.plot(edens_sk, dens_sk)
        factor = int(0.05/0.01)
        axx.plot(E_grid[:nEpts:factor],dens_sk/np.array([i for k,i in enumerate(list_dens[0][:nEpts]) if k%factor==0]),label='dens Skouteris over ' + list_names_dens[0])
        axx.legend()
        #axx.set_yscale("log")
    #
    if not isinstance(mask_ts,np.ndarray) and not isinstance(mask_ts,list) and mask_ts==None:
        mask_ts = (E_grid >= E_grid[0]) & (E_grid <= E_grid[nEpts])
    for i in range(len(list_sums)):
        l=ax[1].plot(E_grid[mask_ts],list_sums[i][mask_ts],label=list_names_sums[i])
        l[0].set_color(cm(i/NUM_C_sums))
        l[0].set_linestyle(LINE_STYLES[int(i%NUM_STYLES)])
    ax[1].set_ylabel("Sum of states TS")
    #
    for a in ax:
        a.set_xlabel(Xaxis_label)
        try:
            a.set_yscale("log")
        except:
            print('could not use log scale!')
            a.set_yscale("linear")
        a.legend()
    #
    return ax
#######################################################################
def plot_kE(E_grid,nEpts, list_kE,list_names_kE, Sk_csv_file=None):
    
    cm = plt.get_cmap('gnuplot')# gist_stern
    LINE_STYLES = ['solid', 'dashed', 'dotted', 'dashdot']
    NUM_STYLES, NUM_C_ke = len(LINE_STYLES), len(list_kE)
    
    fig,axs = plt.subplots(1,2,figsize=(10,5))
    #
    # Read (if Exists) Skouteris' code generated data of kE (cla, tunn) and Energies
    if Sk_csv_file!= None:
        with open(Sk_csv_file,"r") as caca:
            data = caca.readlines()
            kE_Skouteris = np.zeros(len(data))
            kE_Skouteris_t = np.zeros(len(data))
            En_Skouteris = np.zeros(len(data))
            for i,line in enumerate(data):
                kE_Skouteris[i]=line.split()[2]
                kE_Skouteris_t[i]=line.split()[1]
                En_Skouteris[i]=line.split()[0]
        for i in [0,1]:
            axs[i].plot(En_Skouteris, kE_Skouteris, label="Skouteris_cla",c="orange",lw=3)
            axs[i].plot(En_Skouteris, kE_Skouteris_t, label="Skouteris_tun",c="orange",lw=3,ls='--')
    # Plot my data
    for i, (k, kname) in enumerate(zip(list_kE,list_names_kE)):
        if "Beyer" in kname or "BS" in kname or "bs" in kname:
            l = axs[1].plot(E_grid[:nEpts]/5.03445e22, k[:nEpts], label=kname, lw=2)
        else:
            l = axs[0].plot(E_grid[:nEpts]/5.03445e22, k[:nEpts], label=kname ,lw=2)
        l[0].set_color(cm(i/NUM_C_ke))
        l[0].set_linestyle(LINE_STYLES[int(i%NUM_STYLES)])
    
    for i in [0,1]:
        try:
            axs[i].set_yscale("log")
        except:
            print('could not use log scale!')
            axs[i].set_yscale("linear")
        axs[i].set_xlabel("Energy (J)")
        axs[i].set_ylabel(r"k(E) [s$^{-1}$]")
        axs[i].legend()

    plt.tight_layout()
    return axs
#######################################################################
def plot_kT(maxT,list_kT,list_names_kT,Sk_csv_file=None):
#def plot_kT(maxT,kT,kT_tun,Sk_csv_file=None):

    cm = plt.get_cmap('gist_stern')# gist_stern, gnuplot
    LINE_STYLES = ['solid', 'dashed', 'dotted', 'dashdot']
    NUM_STYLES, NUM_C_kt = len(LINE_STYLES), len(list_kT)
                                       
    plt.rcParams.update({'font.size': 16})
    fig,axs = plt.subplots(1,2,figsize=(10,5))
    #
    if Sk_csv_file != None:
        with open(Sk_csv_file,"r") as caca:
            data = caca.readlines()
            kT_Skouteris = np.zeros(len(data))
            kT_Skouteris_t = np.zeros(len(data))
            T_Skouteris = np.zeros(len(data))
            Tinv_Skouteris = np.zeros(len(data))
            for i,line in enumerate(data[1:]):
                kT_Skouteris[i]  =line.split(",")[4]
                kT_Skouteris_t[i]=line.split(",")[3]
                T_Skouteris[i]   =line.split(",")[1]
                Tinv_Skouteris[i]=line.split(",")[2]
        axs[0].plot(T_Skouteris[:-1], kT_Skouteris[:-1]  , label="Skouteris_cla",c="orange",lw=3)
        axs[0].plot(T_Skouteris[:-1], kT_Skouteris_t[:-1], label="Skouteris_tun",c="orange",ls="--",lw=3)
        axs[1].plot([1./t for t in T_Skouteris[:-1]], kT_Skouteris[:-1], label="Skouteris_cla",c="orange",lw=3)
        axs[1].plot([1./t for t in T_Skouteris[:-1]], kT_Skouteris_t[:-1], label="Skouteris_tun",c="orange",ls="--",lw=3)
    #
    for i, (k, kname) in enumerate(zip(list_kT,list_names_kT)):
        l  = axs[0].plot([(t+1.)     for t in range(maxT)], k, label=kname,lw=3)
        ll = axs[1].plot([1./(t+1.)  for t in range(maxT)], k, label=kname,lw=3)
        l[0].set_color(cm(i/NUM_C_kt))
        l[0].set_linestyle(LINE_STYLES[int(i%NUM_STYLES)])
        ll[0].set_color(cm(i/NUM_C_kt))
        ll[0].set_linestyle(LINE_STYLES[int(i%NUM_STYLES)])
    #
    for i in [0,1]:
        try:
            axs[i].set_yscale("log")
        except:
            print('Could not use log scale!')
            axs[i].set_yscale("linear")
        #axs[i].set_ylim([1e-10,1e9])
        #axs[i].set_xlim([1./150.,1./5.])
        axs[i].set_ylabel("k(T) [1/s]")
        axs[i].legend()
    axs[0].set_xlabel("Temperature [K]")
    axs[1].set_xlabel("Inverse Temperature [1/K]")
    #
    plt.tight_layout()
    return axs

def add_Xaxis_EoverEref(ax,Eref):
    # ax must be a list
    dummy_axes = [ax[0].twiny(),ax[1].twiny()]
    for i in [0,1]:
        dummy_axes[i].set_xticks( ax[i].get_xticks() )
        dummy_axes[i].set_xbound( ax[i].get_xbound() )
        dummy_axes[i].set_xticklabels( [int(e/Eref) for e in ax[i].get_xticks()])
        dummy_axes[i].set_xlabel("E/Ets")


