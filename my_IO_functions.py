import os, sys
import numpy as np

def STORE_into_CSV(path,cols,header):
    ncols = len(cols)
    
    if not all(v == len(cols[0]) for v in [len(i) for i in cols]):
        print("ATTENTION: In order to save a CSV all columns must be the same length ({})".format(path))
    else:
        with open(path,'w') as ofile:
            ofile.write('# '+str(header)+'\n')
            for ind in range(len(cols[0])):
                row = [str(i[ind]) for i in cols]
                cacarella = ",    ".join(row)
                ofile.write(cacarella+'\n')
                # print(cacarella)

def FIND_LINE(mystr,data=[-1],path=None):
    # Will look for mystr (str or list of str) in 
    #  data or path line per line.
    #print("mystr",mystr)
    if type(mystr) is str:
        leninp = 1
        mystr  = [mystr]
    elif isinstance(mystr,list) or isinstance(mystr,np.ndarray):
        leninp = len(mystr)
    else:
        print("!! Introduce a valid set of mystr (1 or a list)")
    #
    found=[-1]*leninp
    #
    if data==[-1]:
        with open(path,"r") as inp:
            data = inp.readlines()
    elif data==[-1] and path==None:
        print("!! Introduce a valid data list OR file path")
    #
    for ic, case in enumerate(mystr):
        for il, line in enumerate(data):
            if case in line.strip()[0:len(case)] and line.strip()[0]!="#" and found[ic]==-1 and il not in found:
                found[ic] = il
    if leninp==1:
        if found[0]!=-1: return found[0]
        else: return -1
    elif leninp>1 and -1 not in found: return found

def READ_VALUE_FROM_INP(str_var,data):
    # takes in a string (variable name, as it appears in input), and returns it value
    lnr = FIND_LINE(str_var,data=data)
    # print("lnr",lnr)
    if lnr != -1 and data[lnr].strip()[0]!="#":
        element = data[lnr].strip().split(":")[1]
        if "#" in element:
            element = element.split("#")[0]
        if "," in element:
            element=[i.strip() for i in element.split(",")]
        if type(element) is list: return element
        else: return element.strip()
    else:
        print("Variable {} not found! Default value taken.".format(str_var))
def READ_BOOLEAN_FROM_INP(str_val,data):
    # print(READ_VALUE_FROM_INP(str_val,data=data))
    if READ_VALUE_FROM_INP(str_val,data=data) in ['True','true','TRUE','t','T','1','yes','Yes','YES']:
        return True
    elif READ_VALUE_FROM_INP(str_val,data=data) in ['False','false','FALSE','f','F','0','no','No','NO']:
        return False
    else:
        print("!! Introduce a correct BOOLEAN for {} (True/False)".format(str_val))
def READ_VIB_MODES(data):
    # Reads input data (path/nr col) and returns list of frqs by reading from file (CSV)
    #
    path_vib_rg = READ_VALUE_FROM_INP("vibr_rg",data)
    path_vib_ts = READ_VALUE_FROM_INP("vibr_ts",data)
    #
    col_rg,col_ts=-1,-1
    #
    if type(path_vib_rg) is list:
        path_rg = path_vib_rg[0]
        col_rg  = int(path_vib_rg[1])-1
    else: path_rg = path_vib_rg
    if type(path_vib_ts) is list:
        path_ts = path_vib_ts[0]
        col_ts  = int(path_vib_ts[1])-1
    else: path_ts = path_vib_ts
    #
    #print("path_rg",path_rg)
    #print("path_ts",path_ts)
    if path_rg==path_ts:
        vibs = np.genfromtxt(path_rg, delimiter=',', comments="#").transpose() # units must be cm-1
        return vibs[col_rg], vibs[col_ts]
    #
    elif path_rg!=path_ts:
        if col_rg!=-1: v_rg = np.loadtxt(path_rg, delimiter=',', comments="#",usecols=(col_rg))
        else:          v_rg = np.loadtxt(path_rg, delimiter=',', comments="#")
        if col_ts!=-1: v_ts = np.loadtxt(path_ts, delimiter=',', comments="#",usecols=(col_ts))
        else:          v_ts = np.loadtxt(path_ts, delimiter=',', comments="#")
        return v_rg, v_ts

def READ_INPUT(path):
    '''
    Will read data from input file and provide the variables in
     Dictionaries (one for Global vars and a list of dirs for the
     different Steps), in the correct units.
    Input: all energies must be in kJ/mol and will provide them in cm-1
           frqs vib in cm-1 and rot_cts in GHzs
    '''
    kJmol_to_cm1=83.59345392546533 
    if os.path.isfile(path):
        ## GLOBAL SECTION 
        ini = FIND_LINE("-- GLOBAL --",path=path)
        fin = FIND_LINE("-- STEP 1 --",path=path)
        with open(path,"r") as inp: data = inp.readlines()
        data_glob = data[ini:fin].copy()
        #
        if FIND_LINE("dE",path=path)!=-1:
            dE    = float(READ_VALUE_FROM_INP("dE",data=data_glob))   * kJmol_to_cm1
        else:
            dE = 0.05 * kJmol_to_cm1
            print("Default energy grain is {:.2f} cm-1 = {:.4f} kJ/mol".format(dE,dE/kJmol_to_cm1))
        if FIND_LINE("nEpts",path=path)!=-1:
            nEpts = int(READ_VALUE_FROM_INP("nEpts",data=data_glob))
        else:
            nEpts = 3000
            print("Default number of E grains is",nEpts)
        if FIND_LINE("Ein",path=path)!=-1:
            Ein   = float(READ_VALUE_FROM_INP("Ein",data=data_glob))  * kJmol_to_cm1
        else:
            Ein = dE
            print("Default Ein is",Ein,"cm-1")
        if FIND_LINE("Emin",path=path)!=-1:
            Emin  = float(READ_VALUE_FROM_INP("Emin",data=data_glob)) * kJmol_to_cm1
        else:
            Emin = 0.0
            print("Default Emin is",Emin)
        if FIND_LINE("maxT",path=path)!=-1:
            maxT  = int(READ_VALUE_FROM_INP("maxT",data=data_glob))
        else:
            maxT = 150
            print("No maxT selected, a value of",maxT,"K is used by default")
        if FIND_LINE("num_steps",path=path)!=-1:
            num_steps = int(READ_VALUE_FROM_INP("num_steps",data=data_glob))
        else:
            num_steps = 1
            print("Please introduce the number of steps in your PES, otherwise I consider there to be only 1.")
        if FIND_LINE("ROT_Dens",path=path)!=-1:
            ROT_Dens = READ_BOOLEAN_FROM_INP("ROT_Dens",data=data_glob)
        else:
            ROT_Dens=False
            print("No rotational constants given, I will only use vibrational levels.")
        if FIND_LINE("TUN",path=path)!=-1:
            tun_fl = READ_VALUE_FROM_INP("TUN",data=data_glob)
        else:
            tun_fl="no"
            print("No tunneling selected. I will only calculate classical rates.")
        if type(tun_fl) is str:
            if tun_fl == 'all':
                TUN = list(range(num_steps))
            elif tun_fl == 'no':
                TUN = [-1]
            else:
                TUN = [int(tun_fl)-1]
        elif type(tun_fl) is list:
            TUN = [int(i)-1 for i in tun_fl]
        if FIND_LINE("dir_out",path=path)!=-1:
            dir_out = str(READ_VALUE_FROM_INP("dir_out",data=data_glob))
        else:
            dir_out=os.getcwd()+'/OUT'
            print('Default outdir is',dir_out)
        if FIND_LINE("Printing_level",path=path)!=-1:
            printlevel = str(READ_VALUE_FROM_INP("Printing_level",data=data_glob))
            if printlevel in ["D","d","Debug","debug","Debuging","debuging",
                              "Debugging","debugging","deb","Deb","high","High","h","H"]:
                printlevel = "D"
            elif printlevel in ["N","n","Normal","normal"]:
                printlevel="N"
        else:
            printlevel = "N"
            print('Default printing level is Normal')
        if FIND_LINE("Sanity_Check",path=path)!=-1:
            sanity_check = READ_BOOLEAN_FROM_INP("Sanity_Check",data=data_glob)
        else:
            sanity_check = "True"
            print('Default sanity check is True')
        #
        GLOBALS = {"dE":dE,"nEpts":nEpts,"Ein":Ein,"Emin":Emin,"maxT":maxT,
                   "num_steps":num_steps,"ROT_Dens":ROT_Dens,
                   "TUN":TUN,"dir_out":dir_out,"Printing_level":printlevel,"Sanity_Check":sanity_check}
        ### Create out dir
        if not os.path.exists(dir_out):
          # Create a new directory because it does not exist 
          os.makedirs(dir_out)
        
        ###
        del data_glob
        #
        DATA_STEPS = [-1]*num_steps
        #
        ## STEPS
        for ist, step in enumerate(range(num_steps)):
            step_nr = ist + 1
            ini = FIND_LINE("-- STEP {} --".format(step_nr),data=data)
            if step_nr+1<num_steps: fin = FIND_LINE("-- STEP {} --".format(step_nr+1),data=data)
            else:                   fin = len(data)
            #
            ## DEFAULTS
            name = "step_{}".format(ist+1)
            #
            data_step = data[ini:fin].copy()
            #
            name   = str(READ_VALUE_FROM_INP("name",data_step))
            vib_rg, vib_ts = READ_VIB_MODES(data_step)
            if ROT_Dens != False:
                Rct_rg = [float(i) for i in READ_VALUE_FROM_INP("Rct_rg",data=data_step)]
                Rct_ts = [float(i) for i in READ_VALUE_FROM_INP("Rct_ts",data=data_step)]
            else:
                Rct_rg=None
                Rct_ts=None
            Erg    = float(READ_VALUE_FROM_INP("Erg",data=data_step)) * kJmol_to_cm1 # convert kJ/mol to cm-1
            Ets    = float(READ_VALUE_FROM_INP("Ets",data=data_step)) * kJmol_to_cm1
            Epr    = float(READ_VALUE_FROM_INP("Epr",data=data_step)) * kJmol_to_cm1
            #
            DATA_STEPS[ist] = {"name": name, "vibr_rg": vib_rg, "vibr_ts":vib_ts,
                               "Rct_rg":Rct_rg, "Rct_ts":Rct_ts, "Erg":Erg,
                               "Ets":Ets, "Epr":Epr}
            #
            del data_step
        print("NOTE: kJ/mol were converted into cm-1")
        return GLOBALS, DATA_STEPS
    else:
        print("!! Could not find path:", path)
        return -1,-1

    
